# warmup

### Challenge Description

    Difficulty : Easy

### Short Writeup

Leak canary and stack with printf vulnerability, calculate the gaurd cookie of main stack frame, overwrite return address.

### Flag
inctf{Ok4y..._Thats_b3autiful-_=!!!}

@slashb4sh
