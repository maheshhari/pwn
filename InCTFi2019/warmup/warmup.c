#include<Windows.h>
#include<stdio.h>

HANDLE hStdout;
HANDLE hStdin;
int func()
{
    char b[30]="Tell me what you want :";
    HANDLE hHeap = GetProcessHeap();
    LPSTR buf = HeapAlloc(hHeap, HEAP_ZERO_MEMORY, 0x150);
    printf(b);
    ReadFile(hStdin, buf, 0x150, NULL, NULL);
    printf(buf);
    printf("\n");
    return 0;
}

int init()
{
    hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    hStdin = GetStdHandle(STD_INPUT_HANDLE);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
    return 0;
}

int catFlag()
{
    DWORD nIn, nOut;
    char buffer[0x20];
    HANDLE flag = CreateFile("./flag", GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    while (ReadFile(flag, buffer, 0x20, &nIn, NULL) && (nIn != 0) && WriteFile(hStdout, buffer, nIn, &nOut, NULL));
    return 0;
}

int main()
{
    init();
    printf("Welcome ------ Banner\n");
    char buffer[0x40];
    func();
    printf("Did you make something out of it ??? :");
    ReadFile(hStdin, buffer, 0x60, NULL, NULL);
    return 0;
}