#include<Windows.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define LEN 20
#define NAME_SIZE 0x20

HANDLE hStdin;
HANDLE hStdout;

BOOL isPrime(int n);
int nthPrime(int n);
int initialize(); 
void addIngred(char *name);
void listIngred();
void makeDrink();
void stackUp();

struct ingredient{
	char *name;
	DWORD key;
};

struct drink{
	char *name;
	DWORD value;
};

struct ingredient shelf[LEN];
struct drink *drinks[LEN];

BOOL isPrime(int n) 
{ 
    if (n <= 1) 
        return FALSE; 
    for (int i = 2; i < n; i++) 
        if (n % i == 0) 
            return FALSE; 
    return TRUE; 
} 

int nthPrime(int n)
{
	int c=0;
	int num=2;
	while(1)
	{
		if(isPrime(num)==TRUE)
		{
			c++;
			if(c==n)
			{
				return num;
			}
		}
		num++;
	}
}

void listDrinkIngred(int n)
{
	int i=1;
	int j=0;
	int val = drinks[n]->value;
	puts("\tIngredients in Drink");
	for(;i<LEN && shelf[i].name!=NULL;i++)
	{
		if(val%shelf[i].key==0)
		{
			j++;
			printf("\t\t%d. %s\n", j, shelf[i].name);
		}
	}
}

void listDrink()
{
	int i=0;
	puts("Drinks :");
	for(; i<LEN && drinks[i]!=NULL; i++)
	{
		printf("\t%d. %s\n\tPrice: %d\n", i, drinks[i]->name, drinks[i]->value);
		listDrinkIngred(i);
	}
}

void listIngred()
{
	int i=0;
	puts("Ingredients :");
	for(; i<LEN && shelf[i].name!=NULL; i++)
	{
		printf("\t%d. %s\n", i, shelf[i].name);
	}
}

void makeDrink()
{
	char nameT[NAME_SIZE];
	DWORD nIn=0;
	int i=0;
	for(; i<LEN && drinks[i]!=NULL; i++);
	if(i<LEN)
	{
		DWORD option;
		HANDLE hHeap = GetProcessHeap();
		struct drink *temp = (struct drink *)HeapAlloc(hHeap, HEAP_ZERO_MEMORY, sizeof(struct drink));
		char *nameD = (char *)HeapAlloc(hHeap, HEAP_ZERO_MEMORY, NAME_SIZE);
		printf("Enter Drink name : ");
		ReadFile(hStdin, nameT, NAME_SIZE-1, &nIn, NULL);
		if(nIn==0)
			exit(0);
		nameT[nIn-1]=0;
		strncpy_s(nameD, NAME_SIZE, nameT, NAME_SIZE-1);
		temp->name=nameD;
		temp->value = 1;
		while(1)
		{
			listIngred();
			puts("\t99. leave menu");
			printf("Select the ingredients : ");
			scanf_s("%d", &option);
			if(option==99)
				break;
			if(option==0)
			{
				temp->value=1;
			}
			else 
			{
				temp->value *= shelf[option].key;
			}
			printf("Current price = %d\n", temp->value);
		}
		drinks[i]=temp;
	}
}

void addIngred(char *n)
{
	int i=0;
	for(; i<LEN && shelf[i].name!=NULL; i++);
	if(i<LEN)
	{
		HANDLE hHeap = GetProcessHeap();
		char *tempN = (char *)HeapAlloc(hHeap, HEAP_ZERO_MEMORY, NAME_SIZE); 
		strncpy_s(tempN, NAME_SIZE-1,n,NAME_SIZE-1);
		shelf[i].name = tempN;
		shelf[i].key = nthPrime(i);
	}
	else
	{
		puts("----The shelf is full----");
	}
}

int initialize() {
	stackUp();
	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	hStdin = GetStdHandle(STD_INPUT_HANDLE);
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);
	return 0;
}

void stackUp()
{
	HANDLE hHeap = GetProcessHeap();
	char *tempN = (char *)HeapAlloc(hHeap, HEAP_ZERO_MEMORY, NAME_SIZE); 
	strncpy_s(tempN, NAME_SIZE , "Clear", 5);
	shelf[0].name = tempN;
	shelf[0].key = 0;
	addIngred("Whiskey");
	addIngred("Vodka");
	addIngred("Gin");
	addIngred("Tequila");
	addIngred("Rum");
	addIngred("Carbonated water");
	addIngred("lemon juice");
	addIngred("cola");
	addIngred("mint");
}

void serveDrinks()
{
	int i=0;
	for(; i<LEN && drinks[i]!=NULL; i++)
	{
		drinks[i]=NULL;
	}
	puts("Enjoy your drinks");
}

void addDrinkIngred(int n)
{
	DWORD option;
	listIngred();
	puts("\t99. leave menu");
	printf("Select the ingredients to add : ");
	scanf_s("%d", &option);
	if(option==99)
		return;
	if(option==0)
	{
		drinks[n]->value=1;
	}
	else
	{
		drinks[n]->value *= shelf[option].key;
	}
	printf("Current price = %d\n", drinks[n]->value);

}

void remDrinkIngred(int n)
{
	DWORD option;
	listIngred();
	puts("\t99. leave menu");
	printf("Select the ingredients to remove : ");
	scanf_s("%d", &option);
	if(option==99)
		return;
	drinks[n]->value /= shelf[option].key;
	printf("Current price = %d\n", drinks[n]->value);
}

int catFlag()
{
    DWORD nIn, nOut;
    char buffer[0x30];
    HANDLE flag = CreateFile("./flag", GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    while (ReadFile(flag, buffer, 0x30, &nIn, NULL) && (nIn != 0) && WriteFile(hStdout, buffer, nIn, &nOut, NULL));
    return 0;
}

void changeDrink()
{
	if(drinks[0]==NULL)
	{
		puts("----No drinks available----");
		return;
	}
	DWORD option;
	DWORD choice;
	listDrink();
	printf ("Which drink do you want to change : ");
	scanf_s("%d", &option);
	if(option>LEN || drinks[option]==NULL)
	{
		puts("----Select a drink----");
		return;
	}
	puts("\t1. Add Ingredient");
	puts("\t2. Remove Ingredient");
	puts("What change : ");
	scanf_s("%d", &choice);
	switch(choice) {
		case 1: addDrinkIngred(option);
			break;
		case 2: remDrinkIngred(option);
			break;
		default : return;
	}
}


int main() {
	puts("Welcome!! Hope you have a good time");
	initialize();
	int ch;
	char nameI[NAME_SIZE];
	while(1) {
		puts("What would you like");
		puts("\t1. Make a drink");
		puts("\t2. Look at menu");
		puts("\t3. Change a drink");
		puts("\t4. Serve drinks");
		puts("\t5. Add an Ingredient");
		puts("\t6. Leave bar");
		printf("Enter your choice : ");
		scanf_s("%d",&ch);
		switch(ch) {
			case 1: makeDrink();
				break;
			case 2: listDrink();
				break;
			case 3: changeDrink();
				break;
			case 4: serveDrinks();
				break;
			case 5: printf("Enter the name of the ingredient : ");
					ReadFile(hStdin, nameI, 0x100, NULL, NULL);
					nameI[NAME_SIZE-1] = 0;
					addIngred(nameI);
				break;

			default: exit(0);
					catFlag();
		}
	}
}