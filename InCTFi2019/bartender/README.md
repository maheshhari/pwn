# bartender

### Challenge Description

    Difficulty : Easy/Medium

### Short Writeup

Overwrite exception handler and trigger exception with division by zero.

### Flag
inctf{000-z3r0_iS_An_ExCept1on4l_nuMb3r-000}

@slashb4sh
